from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views as views
from todos.views import TodosListView, TodosDeleteView, TodosDeleteView, TodosCreateView, TodosList

from todos.views import (
    TodosListView,
    TodosDetailView,
    TodosCreateView,
    TodosDeleteView,
    TodosUpdateView,
)
urlpatterns = [
    path("", TodosListView.as_view(), name="todos_list_list"),
    # path("todos/", include("todos.urls")),
    path("<int:pk>/", TodosDetailView.as_view(), name="todos_list_detail"),
    path("<int:pk>/delete/", TodosDeleteView.as_view(), name="todos_list_delete"),
    path("new/", TodosCreateView.as_view(), name="todos_list_create"),
    path("<int:pk>/edit/", TodosUpdateView.as_view(), name="todos_list_edit"),
    path('', views.TodosList, name="todos_list_list")
]
