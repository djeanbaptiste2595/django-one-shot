from django.contrib import admin

# Register your models here.
from todos.models import TodosList, TodosItem


class TodosListAdmin(admin.ModelAdmin):
    pass


class TodosItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodosList, TodosListAdmin)


admin.site.register(TodosItem, TodosItemAdmin)
