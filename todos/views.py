from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView, CreateView, UpdateView

from todos.models import TodosList
from django.http import HttpResponse


def Todoslist(request):
    return HttpResponse("TodosList", {"success": True})

# ( I createed a function base view to connect the Url patterns together. To
# do that, we needto go into our views and render out a simple function
# base view.)


class TodosListView(ListView):
    model = TodosList
    template_name = 'todos/todoslist.html'


class TodosDetailView(DetailView):
    model = TodosList
    template_name = 'todos/todosdetail.html'


class TodosDeleteView(DeleteView):
    model = TodosList
    template_name = 'todos/todosdelete.html'


class TodosCreateView(CreateView):
    model = TodosList
    template_name = 'todos/todosnew.html'
    fields = ["name"]
    success_url = reverse_lazy("todos_list_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class TodosUpdateView(UpdateView):
    model = TodosList
    template_name = 'todos/edit.html'
